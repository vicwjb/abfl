# entity-utils.lsp

## BF-ent-addpline

说明：
生成二维多段线

参数：

* plist - 端点坐标点表，如：((x1 y1 z1) (x2 y2 z2) (x2 y2 z2))或((x1 y1) (x2 y2) (x2 y2))

* tudulist - 各点与下一点的凸度，可为nil

* bg - 标高

* clo - 是否闭合，1

返回值: 
生成多段线的图元名

示例:
```
(BF-ent-addpline '((102.946 68.6354 3) (112.102 97.4851 3) (125.484 59.4879 3) (103.651 52.4513 3)) '(-1.07092 -0.685629 0 -0.31201) 211 1))
 ```

## BF-ent-addarrow

说明：
生成箭头

参数：

* startpt - 箭头尖坐标

* endpt - 箭头尾坐标

* width - 箭头尾宽度

返回值: 
箭头图元名

示例:
```
(BF-ent-addarrow (getpoint)(getpoint) 3)
 ```

## BF-ent-addarc

说明：
创建圆弧

参数：

* cen - 圆心

* rad - 半径

* startpt - 起点坐标或弧度

* endpt - 终点坐标或弧度

返回值: 
圆弧图元名

示例:
```
(BF-ent-addarc (getpoint) 3 (getpoint) (getpoint))
 ```

## BF-ent-addcircle

说明：
创建圆

参数：

* cen - 圆心

* rad - 半径

返回值: 
圆图元名

示例:
```
(BF-ent-addcircle (getpoint) 3)
 ```

## BF-ent-addline

说明：
创建直线

参数：

* startpt - 起点坐标

* endpt - 终点坐标

返回值: 
直线图元名

示例:
```
(BF-ent-addline (getpoint) (getpoint))
 ```

## BF-ent-addlines

说明：
创建连续直线

参数：

* pts - 坐标表

返回值: 
图元名列表

示例:
```
(BF-ent-addlines (list pt1 pt2 pt3 ...))
 ```

## BF-ent-getbox

说明：
图元的最小包围盒

参数：

* ent - 图元名

* offset - 外框偏移距离

 + 等于0 / nil，不偏移

 + 大于0，向外偏移

 + 小于0，向内偏移

返回值: 
外框（偏移后）的左下，右上角点

示例:
```
(BF-ent-getbox (car(entsel)) 0.1)
 ```

## BF-Ent-getTextBox

说明：
创建直线

参数：

* text - 文字图元名

* offset - 外框偏移距离

 + 等于0 / nil，不偏移

 + 大于0，向外偏移

 + 小于0，向内偏移

返回值: 
文字外框（偏移后）的四个角点（左下，右下，右上，左上）

示例:
```
(BF-Ent-getTextBox (car(entsel)) 2)
 ```

## BF-ent-addtext

说明：
生成一个TEXT实体,BF-ent-maketext参数简化版

参数：

* text - 文字

* pt - 文字基点

* zg - 字高

* ang - 旋转角度，以(弧度)为单位

* dq - 对齐样式

 + 0 : 中心

 + 11：左上

 + 12：左中

 + 13：左下

 + 21：中上

 + 22：正中

 + 23：中下

 + 31：右上

 + 32：右中 

 + 33：右下

返回值: 
文字图元名

示例:
```
(BF-ent-addtext "文字" (getpoint) 3 0 11)
 ```

## BF-ent-maketext

说明：
生成一个TEXT实体,By Longxin 明经通道 2006.04

参数：

* text - 文字

* pt - 文字基点

* zg - 字高

* ang - 旋转角度，以(弧度)为单位

* kgb - 宽高比

* qx - 倾斜

* dq - 对齐样式

 + 0 : 中心

 + 11：左上

 + 12：左中

 + 13：左下

 + 21：中上

 + 22：正中

 + 23：中下

 + 31：右上

 + 32：右中 

 + 33：右下

返回值: 
文字图元名

示例:
```
(BF-ent-maketext "文字" (getpoint) 3 0 0.8 0 11)
 ```

## BF-ent-adddim

说明：
生成标注

参数：

* start - 标注起点

* end - 标注端点

* distpt - 标注尺寸线方位点

返回值: 
标注图元名

示例:
```
(BF-ent-adddim (getpoint) (getpoint) (getpoint))
 ```

## BF-ent-addRectangle

说明：
构造矩形 by highflybird

参数：

* pt1 - 左下坐标

* pt2 - 右上坐标

返回值: 
矩形多段线图元名

示例:
```
(BF-ent-addRectangle (getpoint) (getpoint))
 ```

## BF-ent-spline

说明：
根据点表画样条曲线

参数：

* pts - 点表

返回值: 
样条曲线图元名

示例:
```
(BF-ent-spline (list pt1 pt2 pt3 ...))
 ```

## BF-ent-getdxf

说明：
获取图元的组码值

参数：

* ent - 图元名或vla对象名

* i - 组码或组码表

返回值: 
组码值或列表

示例:
```
(BF-ent-getdxf (car (entsel)) 10)
 ```

## BF-ent-putdxf

说明：
更新图元的组码值,根据院长的代码加工了一下

参数：

* ename - 图元，选择集，图元列表

* code - 组码或组码表

* val - 值或者值表

返回值: 
更新后的图元，选择集，图元列表

示例:
```
(BF-ent-putdxf (car (entsel)) 10 '(0 0 0))
 ```

## BF-ent-Offset

说明：
图元偏移

参数：

* obj - 图元名或vla图元对象

* dis - 偏移距离，根据正负决定偏移方向

返回值: 
偏移后的对象

示例:
```
(BF-ent-Offset	(car (entsel)) 0.5)
 ```

## BF-ent-gettable

说明：
返回包含在指定符号表中的所有元素

参数：

* s - 一个符号表名称字符串

 + LAYER:图层

 + LTYPE:线型

 + VIEW：视图

 + STYLE：字体样式

 + BLOCK：块

 + UCS：用户坐标系

 + APPID：

 + DIMSTYLE：标注样式

 + VPORT：视口

返回值: 
元素列表

示例:
```
(BF-ent-gettable "ltype")
 ```

## BF-ent-Layers

说明：
获取图层列表

参数：

* No arguments

返回值: 
图层名列表

示例:
```
(BF-ent-Layers)
 ```

## BF-Ent-LineTypes

说明：
获取图层列表

参数：

* No arguments

返回值: 
图层名列表

示例:
```
(BF-Ent-LineTypes)
 ```

## BF-Ent-TextStyles

说明：
获取文字样式列表

参数：

* No arguments

返回值: 
文字样式名列表

示例:
```
(BF-ent-TextStyles)
 ```

## BF-ent-onlockedlayer

说明：
判断图元是否位于锁定图层

参数：

* ename - 图元名

返回值: 
位于锁定图层，t；反之nil

示例:
```
(BF-ent-onlockedlayer (car (entsel)))
 ```

## BF-Ent-ChangeTextStyle

说明：
更改指定字体样式的字体

参数：

* TextStyleName - 字体样式名称

* FontName - 字体名字

* BigFontName - 大字体名字

返回值: 
无

示例:
```
(BF-Ent-ChangeTextStyle "STANDARD" "SIMfang.TTF" "")
 ```

示例:
```
(BF-Ent-ChangeTextStyle "STANDARD" "simplex.shx" "dayuxp.shx")
 ```

## BF-Ent-Check-Error-Codes

说明：
消除字体乱码，利用gbenor.shx gbcbig.shx

参数：

* doc - 当前活动文档

返回值: 
无

示例:
```
(BF-Ent-Check-Error-Codes (BF-active-document))
 ```

## BF-Ent-DelSameEnt

说明：
删除重复图元

参数：

* ss - 选择集

返回值: 
无

示例:
```
(BF-Ent-DelSameEnt (ssget))
 ```

## BF-Ent-MakeLayer

说明：
创建图层

参数：

* strName - 图层名

* intColor - 图层颜色

* strLtype - 图层线型

* booleCur - 是否置为当前图层

返回值: 
成功返回图层名，失败返回nil

示例:
```
(BF-Ent-MakeLayer "Layer1" 3 "DASHED" T)
 ```

## BF-Ltype-Exists

说明：
线型是否存在?

参数：

* strLtype - 线型名

返回值: 
成功返回t，失败返回nil

示例:
```
(BF-Ltype-Exists "continuous")
 ```

## BF-ent-change-Ltype

说明：
改变对象线型

参数：

* obj - 对象

* strLtype - 线型

返回值: 
成功返回图层名，失败返回nil

示例:
```
(BF-ent-change-Ltype cirobj "DASHED")
 ```

## BF-ent-ActiveLayer

说明：
设置指定层为当前层

参数：

* name - 图层名

返回值: 
成功返回t，失败返回nil

示例:
```
(BF-ent-ActiveLayer "layer1")
 ```

## BF-ent-LayerOn

说明：
图层列表开关函数

参数：

* LayList - 图层名

* flag - 标志位，t打开图层，nil关闭图层

返回值: 


示例:
```
(BF-ent-LayerOn "layer1" t)
 ```

## BF-ent-LayerOn

说明：
图层列表冻结开关函数

参数：

* LayList - 图层名

* flag - 标志位，t冻结图层，nil解冻图层

返回值: 


示例:
```
(BF-ent-Freeze "layer1" t)
 ```

## BF-ent-Plottable

说明：
图层打印开关函数

参数：

* LayList - 图层名

* flag - 标志位，t可打印，nil不可打印

返回值: 


示例:
```
(BF-ent-Plottable "layer1" t)
 ```

## BF-ent-LayerLock

说明：
图层锁定开关函数

参数：

* LayList - 图层名

* flag - 标志位，t锁定，nil解锁

返回值: 


示例:
```
(BF-ent-LayerLock "layer1" t)
 ```

## BF-ent-freezelist

说明：
返回冻结图层列表

参数：

* No arguments

返回值: 
冻结图层列表

示例:
```
(BF-ent-freezelist)
 ```

## BF-Ent-LayerOffList

说明：
返回关闭图层列表

参数：

* No arguments

返回值: 
关闭图层列表

示例:
```
(BF-Ent-LayerOffList)
 ```

## BF-ent-Plottablelist

说明：
返回可打印图层列表

参数：

* No arguments

返回值: 
可打印图层列表

示例:
```
(BF-ent-Plottablelist)
 ```

## BF-ent-freezing

说明：
层是否冻结？

参数：

* lname - 图层名,区分大小写

返回值: 
是-t，否-nil

示例:
```
(BF-ent-freezing "DIM")
 ```

