# string-utils.lsp

## BF-string-add

说明：
字符串相加

参数：

* strlst - 字符串或字符串列表

返回值: 
返回组合成的字符串，参数不符合要求，则返回nil

示例:
```
(BF-string-add '("a" "b" "c"))
 ```

## BF-string-square

说明：
字符串自乘

参数：

* int - 数字

* str - 字符串

返回值: 
返回字符串自乘int次的字符串

示例:
```
(BF-string-square 2 "a")
 ```

## BF-lst->str

说明：
列表转成字符串--leemac

参数：

* lst - 字符串列表

* del - 分隔符

返回值: 
字符串

示例:
```
(BF-lst->str '("a" "b" "c") "-")
 ```

## BF-str->lst

说明：
字符串转成列表--leemac

参数：

* str - 字符串

* del - 分隔符

返回值: 
字符串列表

示例:
```
(BF-str->lst "a-b-c" "-")
 ```

## BF-Str-ParseByLst

说明：
字符串按分隔符列表转列表

参数：

* lstr - 字符串

* DelimLst - 分隔符列表

返回值: 
字符串列表

示例:
```
(BF-Str-ParseByLst "a-b=c" '("-" "="))
 ```

## BF-str-subst-all

说明：
在字符串中搜索替换全部子串--leemac

参数：

* new - 要替换的新子串

* old - 被替换的旧子串

* str - 要处理的字符串

返回值: 
替换后的字符串

示例:
```
(BF-str-subst-all "abc" "qwe" "abcpoilde")
 ```

## BF-str-format

说明：
字符串格式化函数

参数：

* str - 需要格式化的字符串

* formatlist - 格式化内容列表或格式化字符串

返回值: 
格式化后的字符串

示例:
```
(BF-str-format "a{1}b{0}c" '("qwe" "abcpoilde"))
 ```

 + todo:加上复杂的格式化类型

## BF-str-Numberp

说明：
确定字符串是否为数字

参数：

* str - 检查的字符串

返回值: 
字符串为数字为T,反之为nil

示例:
```
(BF-str-Numberp "+.123") 返回 T\n(BF-str-Numberp '1B5') 返回 nil
 ```

## BF-Str-Intp

说明：
确定字符串是否为整数

参数：

* str - 检查的字符串

返回值: 
字符串为整数为T,反之为nil

示例:
```
(BF-Str-Intp "+.123") 返回 nil
 ```

## BF-Str-Realp

说明：
确定字符串是否为实数

参数：

* str - 检查的字符串

返回值: 
字符串为实数为T,反之为nil

示例:
```
(BF-Str-Realp "+.123") 返回 T
 ```

## BF-RegExp-Search

说明：
正则表达式搜索字串

参数：

* string - 被搜索字符串

* express - 正则匹配规则

* key - 字母 i I m M g G的组合字串，i/I = 忽略大小写 m/M = 多行搜索 g/G = 全文搜索

返回值: 
匹配到的字符串表 ((起点位置，长度，字符串)(起点位置，长度，字符串)...)

示例:
```
(BF-RegExp-Search "12345asd66" "asd" "mg")
 ```

## BF-RegExp-RePlace

说明：
正则表达式替换字串

参数：

* string - 被替换字符串

* newstr - 替换的字串

* express - 正则匹配规则

* key - 字母 i I m M g G的组合字串，i/I = 忽略大小写 m/M = 多行搜索 g/G = 全文搜索

返回值: 
替换后的字符串

示例:
```
(BF-RegExp-RePlace "12345asd66" "bvd" "asd" "mg")
 ```

