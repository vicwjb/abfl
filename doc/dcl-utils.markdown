# dcl-utils.lsp

## BF-dcl-Init

说明：
生成dcl文件，并load_dialog

参数：

* label - 对话框标题字符串

* focus - 对话框默认焦点控件

* dclstr - 对话框控件列表

返回值: 
dcl文件名 dcl-id 点对表

示例:
```
(BF-dcl-Init "ceshi" "hah2" f)
 ```

## BF-dcl-Start

说明：
dcl启动函数

参数：

* dlgname - 对话框名

* dcl-id - 由 load_dialog 获得的 DCL 文件标识符。

* actionlist - 由key action点对表组成的动作列表，可为nil

* listbox - 由key lst operation index表组成的listbox列表，可为nil

* sldlist - 由key sld表组成的sldlist列表，可为nil

返回值: 
返回 done_dialog 函数的结束方式参数 status.

示例:
```
(BF-dcl-Start)
 ```

## BF-dcl-End

说明：
结束对话框

参数：

* dclid - dcl文件名 dcl-id 点对表

返回值: 
无

示例:
```
(BF-dcl-End aa)
 ```

## BF-dcl-setvalues

说明：
批量设置对话框控件的值

参数：

* lst - key value 键值对表

 + key 指定控件操作名的字符串。

 + value 字符串，指定控件的新值（初始值由 value 属性设定）。

返回值: 
属性值列表

示例:
```
(BF-dcl-setvalues '(("ket" . "1")("qer" . "2")))
 ```

## BF-dcl-setmodes

说明：
设置对话框控件的状态

参数：

* lst - key mode 键值对表.mode可选项

 + 0 启用控件。

 + 1 禁用控件。

 + 2 将控件设为焦点。

 + 3 选择编辑框内容。

 + 4 切换图像亮显。

返回值: 
nil

示例:
```
(BF-dcl-setmodes '(("ket" . 1)("qer" . 0)))
 ```

## BF-dcl-getvalues

说明：
获取对话框指定控件的当前运行时的值

参数：

* lst - key列表或字符串

返回值: 
控件值字符串表

示例:
```
(BF-dcl-getvalues '("ket" "qer"))
 ```

## BF-dcl-getattrs

说明：
获取对话框指定控件的某个属性值

参数：

* lst - key attr键值对表

返回值: 
字符串表，包含由 DCL 文件为该控件属性定义的初始值。

示例:
```
(BF-dcl-getattrs '(("ket" . "label")("qer" . "label")))
 ```

## BF-dcl-setAction

说明：
为某一对话框控件指定一个动作表达式，用户在对话框中选中这个控件时，就会执行该动作表达式

参数：

* lst - key 动作表达式键值对表

返回值: 


示例:
```
(BF-dcl-setAction '(("buttonkey" "(func args) (func1 arg1 arg2) (done_dialog 4)") ...))
 ```

## BF-dcl-setLayout

说明：
定义布局函数

参数：

* layoutname - 容器名称 - row column等

* layoutlst - 容器属性点对表，无则为nil

* lst - 控件列表

返回值: 
布局表

示例:
```
(BF-dcl-setLayout "column" nil (list d c))
 ```

## BF-dcl-addItem

说明：
定义一个控件

参数：

* itemname - 控件名称

* lst - 定义控件属性的点对表

返回值: 
控件表

示例:
```
(BF-dcl-addItem "button" '((key . "hah")(label . "hello")(width . 2)(action . "(hh)")))
 ```

## BF-dcl-listsplit

说明：
控件属性表处理函数

参数：

* lst - 控件属性表

返回值: 
处理后的符合dcl规范的表

示例:
```
(BF-dcl-listsplit '((key . "hah")(label . "hello")(width . 2)(action . "(hh)")))
 ```

## BF-dcl-PrintDcl

说明：
生成dcl文件

参数：

* x - 控件列表

* file - dcl文件句柄

* n - 缩近等级

返回值: 
无

示例:
```
(BF-dcl-PrintDcl a b 1)
 ```

## BF-dcl-addlist

说明：
添加列表数据

参数：

* key - 列表框key

* lst - 数据列表

* operation - 整数，指定要执行的列表操作的类型。可以指定下列值之一：

 + 1 修改选定列表的内容

 + 2 附加新的列表项

 + 3 删除旧列表，创建新列表（缺省设置）

* index - 整数，指定后续 add_list 调用要修改的列表项。

 + 列表中的第一项序号为 0。如果未指定该参数，则 index 的缺省值为 0。

 + 如果 start_list 不执行修改操作，则忽略 index 参数。

 + operation index 如果不指定则为 nil

返回值: 
无

示例:
```
(BF-dcl-addlist "lst1" '(1 2 3 4) nil nil)
 ```

## BF-dcl-loadsld

说明：
加载幻灯片 by fsxm

参数：

* key - 图像或图像按钮key

* sld - 幻灯片或颜色代码

返回值: 
无

示例:
```
(BF-dcl-loadsld "img1" 2)
 ```

## BF-dcl-checknumber

说明：
检查数字的合法性

参数：

* value - 数字字符串

* error_msg - 错误信息字符串

* range - 模式代码：

 + 0 - 允许任何数字

 + 1 - 拒绝正数

 + 2 - 拒绝负数

 + 4 - 拒绝0

返回值: 
如果数字满足要求，返回数字，否则在对话框显示错误信息

示例:
```
(BF-dcl-checknumber "123" "输入的为负数！" 2)
 ```

## BF-dcl-checkAngle

说明：
检查是否是角度

参数：

* value - 角度字符串

* error_msg - 错误信息

返回值: 
如果满足要求，返回弧度数字，否则在对话框显示错误信息

示例:
```
(BF-dcl-checkAngle "24%%D")
 ```

## BF-dcl-getId

说明：
获取dcl-id

参数：

* dcl_file - dcl文件名，不在cad支持目录下，则提供全路径

返回值: 
如果找不到文件或不能加载则返回nil，反之返回dcl-id

示例:
```
(BF-dcl-getId "test.dcl")
 ```

## BF-dcl-listbox

说明：
列表框对话框控件

参数：

* title - 对话框标题

* msg - 提示字符串

* lst - 数据列表

返回值: 
成功返回选择的字串列表，否则返回nil

示例:
```
(BF-dcl-listbox "选择图层" "图层列表" '("1" "2" "3" "4" "5" "6"))
 ```

