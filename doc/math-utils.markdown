# math-utils.lsp

## BF-math-union

说明：
列表并集

参数：

* lst1 - 列表

* lst2 - 列表

返回值: 
并集运算后的列表

示例:
```
(BF-math-union '(102.946 68.6354 3) '(112.102 97.4851 3))
 ```

## BF-math-subtract

说明：
列表差集

参数：

* lst1 - 列表

* lst2 - 列表

返回值: 
差集运算后的列表

示例:
```
(BF-math-subtract '(102.946 68.6354 3) '(112.102 97.4851 3))
 ```

## BF-math-intersect

说明：
列表交集

参数：

* lst1 - 列表

* lst2 - 列表

返回值: 
交集运算后的列表

示例:
```
(BF-math-intersect '(102.946 68.6354 3) '(112.102 97.4851 3))
 ```

## BF-math-minlist

说明：
计算列表的最小值

参数：

* lst - 列表

返回值: 
列表的最小值

示例:
```
(BF-math-minlist '(102.946 68.6354 3))
 ```

## BF-math-maxlist

说明：
计算列表的最大值

参数：

* lst - 列表

返回值: 
列表的最大值

示例:
```
(BF-math-maxlist '(102.946 68.6354 3))
 ```

## BF-math-power

说明：
增强power函数，目的为扩展expt函数

参数：

* base - 字符串，数字，列表类型，其他类型返回nil

* pow - 整数（用于字符串，列表，数字）实数（用于数字）

返回值: 
参数都为数字时，返回expt计算的结果，base为字符串和列表时，返回自乘的结果

示例:
```
(BF-math-power '(102.946 68.6354 3) 2)
 ```

## BF-math-degress->radions

说明：
角度转弧度函数

参数：

* degress - 角度值，十进制

返回值: 
弧度

示例:
```
(BF-math-degress->radions 45)
 ```

## BF-math-radions->degress

说明：
弧度转角度函数

参数：

* degress - 弧度值

返回值: 
角度

示例:
```
(BF-math-radions->degress 1.2)
 ```

## BF-math-dms

说明：
根据给定十进制角度返回度分秒格式的表

参数：

* degress - 角度值，十进制

返回值: 
度分秒表，数字格式

示例:
```
(BF-math-dms 1.2)
 ```

## BF-math-dmm

说明：
根据给定弧度返回度分秒格式的表

参数：

* degress - 弧度值

返回值: 
度分秒表，字符串格式

示例:
```
(BF-math-dmm 1.2)
 ```

## BF-math-tan

说明：
计算正切值

参数：

* x - 弧度值

返回值: 
正切值

示例:
```
(BF-math-tan 1.2)
 ```

## BF-math-mid

说明：
计算中点

参数：

* x - 第一点坐标

* y - 第二点坐标

返回值: 
中点坐标

示例:
```
(BF-math-mid '(1 2 0) '(2 3 0))
 ```

## BF-math-asin

说明：
计算反正弦值

参数：

* x - 数值，-1 <= x <= 1

返回值: 
弧度值

示例:
```
(BF-math-asin 0.8)
 ```

## BF-math-acos

说明：
计算反余弦值

参数：

* x - 数值，-1 <= x <= 1

返回值: 
弧度值

示例:
```
(BF-math-acos 0.8)
 ```

## BF-math-sinh

说明：
计算双曲正弦值

参数：

* x - 数值

返回值: 
双曲正弦值

示例:
```
(BF-math-sinh 0.8)
 ```

## BF-math-cosh

说明：
计算双曲余弦值

参数：

* x - 数值

返回值: 
双曲余弦值

示例:
```
(BF-math-cosh 0.8)
 ```

## BF-math-tanh

说明：
计算双曲正切值

参数：

* x - 数值

返回值: 
双曲正切值

示例:
```
(BF-math-tanh 0.8)
 ```

## BF-math-arsinh

说明：
计算反双曲正弦值

参数：

* x - 数值

返回值: 
反双曲正弦值

示例:
```
(BF-math-arsinh 0.8)
 ```

## BF-math-arcosh

说明：
计算反双曲余弦值

参数：

* x - 数值，1 <= x

返回值: 
反双曲余弦值

示例:
```
(BF-math-arcosh 1.8)
 ```

## BF-math-artanh

说明：
计算反双曲正切值

参数：

* x - 数值，-1 < x < 1

返回值: 
反双曲正切值

示例:
```
(BF-math-artanh 1.8)
 ```

## BF-math-trim

说明：
数值后续零清除

参数：

* RealNum - 实数

返回值: 
清除后续零的实数

示例:
```
(BF-math-trim 1.8000)
 ```

## BF-math-rtos

说明：
保留小数位数(四舍五入)

参数：

* Real - 实数

* prec - 保留位数

返回值: 
四舍五入后的字符串

示例:
```
(BF-math-rtos 1.8000 3)
 ```

## BF-math-cal

说明：
根据给定表达式计算结果

参数：

* lst1 - 表达式变量表

* lst2 - 表达式给定变量值

* str - 表达式字符串

返回值: 
表达式计算的结果

示例:
```
(BF-math-cal '(a b c) '(1 2 3) "a+b+c")
 ```

 + 计算某个角度(以x轴正向，逆时针)的方位角(以Y轴正向，顺时针)

## BF-math-azimuth

说明：
计算某个角度(以x轴正向，逆时针)的方位角(以Y轴正向，顺时针)

参数：

* ang - 弧度

返回值: 
表示方位角的弧度

示例:
```
(BF-math-azimuth 1.8)
 ```

## BF-math-transpt

说明：
根据世界坐标计算2d用户坐标

参数：

* basept - 基点世界坐标

* startpt - 基点用户坐标

* respt - 待计算世界坐标

返回值: 
待计算用户坐标

示例:
```
(BF-math-transpt pt1 pt2 pt3)
 ```

## BF-math-filelength

说明：
计算文件的行数

参数：

* files - 文件地址,getfiled函数返回的格式

返回值: 
文件的行数

示例:
```
(BF-math-filelength "E:\\tmp.lsp")
 ```

## BF-math-calheight

说明：
根据坡度、pt1、pt2，计算新的pt2

参数：

* pt1 - 基准点

* pt2 - 目标点

* podu - 坡度

返回值: 
目标点的高程

示例:
```
(BF-math-calheight pt1 pt2 pt3)
 ```

