# base-utils.lsp

## BF-acad-object

说明：
返回cad对象，参照lee-mac大神的写法，版权属于lee-mac大神

参数：

* No arguments

返回值: 
返回cad对象

示例:
```
(BF-acad-object)
 ```

## BF-active-document

说明：
返回当前活动文档对象，参照lee-mac大神的写法，版权属于lee-mac大神

参数：

* No arguments

返回值: 
返回当前活动文档对象

示例:
```
(BF-active-document)
 ```

## BF-model-space

说明：
返回模型空间对象，参照lee-mac大神的写法，版权属于lee-mac大神

参数：

* No arguments

返回值: 
返回模型空间对象

示例:
```
(BF-model-space)
 ```

## BF-Layers

说明：
返回图层集合，参照lee-mac大神的写法，版权属于lee-mac大神

参数：

* No arguments

返回值: 
返回图层集合对象

示例:
```
(BF-Layers)
 ```

## BF-LineTypes

说明：
返回线型集合，参照lee-mac大神的写法，版权属于lee-mac大神

参数：

* No arguments

返回值: 
返回线型集合对象

示例:
```
(BF-LineTypes)
 ```

## BF-TextStyles

说明：
返回字体样式集合，参照lee-mac大神的写法，版权属于lee-mac大神

参数：

* No arguments

返回值: 
返回字体样式集合对象

示例:
```
(BF-TextStyles)
 ```

## BF-getinput

说明：
获取输入，结合initget和getkword函数

参数：

* promptstr - 提示字符串

* inplist - 关键字列表

* default - 默认返回关键字，如果没有为nil

返回值: 
返回字符串

示例:
```
(BF-getinput "请输入参数" '("Y" "N") "Y")
 ```

## BF-startundo

说明：
开始撤销编组 -- lee mac

参数：

* doc - 当前活动文档-(BF-active-document)

返回值: 
nil

示例:
```
(BF-startundo (BF-active-document))
 ```

## BF-endundo

说明：
结束编组 -- lee mac

参数：

* doc - 当前活动文档-(BF-active-document)

返回值: 
nil

示例:
```
(BF-endundo (BF-active-document))
 ```

## BF-vlap

说明：
判断是否vla对象?

参数：

* obj - vla对象

返回值: 
vla对象为t，其他为nil

示例:
```
(BF-vlap obj1)
 ```

## BF-stringp

说明：
判断是否字符串?

参数：

* arg - 字符串

返回值: 
字符串为t，其他为nil

示例:
```
(BF-stringp "123")
 ```

## BF-realp

说明：
判断是否实数?

参数：

* arg - 数字

返回值: 
实数为t，其他为nil

示例:
```
(BF-stringp 1.2)
 ```

## BF-enamep

说明：
判断是否图元?

参数：

* arg - 图元名

返回值: 
图元名为t，其他为nil

示例:
```
(BF-enamep obj)
 ```

## BF-variantp

说明：
判断是否变体?

参数：

* arg - 变体名

返回值: 
变体名为t，其他为nil

示例:
```
(BF-variantp obj)
 ```

## BF-picksetp

说明：
判断是否非空选择集?

参数：

* x - 选择集

返回值: 
非空选择集为t，其他为nil

示例:
```
(BF-picksetp obj)
 ```

## BF-intp

说明：
判断是否整数?

参数：

* x - 数字

返回值: 
整数为t，其他为nil

示例:
```
(BF-intp 1)
 ```

## BF-safearrayp

说明：
判断是否为安全数组

参数：

* x - 数组

返回值: 
数组为t，其他为nil

示例:
```
(BF-safearrayp a)
 ```

## BF-ename-listp

说明：
判断是否为图元名列表

参数：

* lst - 图元名列表

返回值: 
图元名列表为t，其他为nil

示例:
```
(BF-ename-listp '(a b c))
 ```

## BF-vla-listp

说明：
判断是否为vla对象列表

参数：

* lst - vla对象列表

返回值: 
vla对象列表为t，其他为nil

示例:
```
(BF-vla-listp '(a b c))
 ```

## BF-string-listp

说明：
判断是否为字符串列表

参数：

* lst - 字符串列表

返回值: 
字符串列表为t，其他为nil

示例:
```
(BF-string-listp '("a" "b" "c"))
 ```

## BF-listp

说明：
判断表是否为真正的表,非nil、非点对表

参数：

* lst - 表

返回值: 
表为t，其他为nil

示例:
```
(BF-listp '("a" "b" "c"))
 ```

## BF-DotPairp

说明：
是否为点对表

参数：

* lst - 点对表

返回值: 
点对表为t，其他为nil

示例:
```
(BF-DotPairp '("a" "b" . "c"))
 ```

## BF-curvep

说明：
是否是曲线

参数：

* obj - 曲线

返回值: 
曲线为t，其他为nil

示例:
```
(BF-curvep a)
 ```

## BF-protect-assign

说明：
符号保护 

参数：

* syms - 变量名列表

返回值: 


示例:
```
(BF-protect-assign '(aaa bbb))
 ```

## BF-unprotect-assign

说明：
符号解除保护 

参数：

* syms - 变量名列表

返回值: 


示例:
```
(BF-unprotect-assign '(aaa bbb))
 ```

## setconst

说明：
定义全局常量

参数：

* key - 全局常量名

* value - 全局常量值

返回值: 
返回一个符号保护的变量

示例:
```
(setconst 'aaa 2)
 ```

## BF-doc-gen

说明：
文档生成函数

参数：

* lspfilename - 要生成文档的lsp文件名，格式为getfiled返回值的格式

返回值: 
生成markdown文件

示例:
```
(BF-doc-gen "E:\\lisptest.lsp")
 ```

## BF-time-start

说明：
计时器开始函数

参数：

* No arguments

返回值: 
计时器全局变量

示例:
```
(BF-time-start)
 ```

## BF-time-end

说明：
计时器结束函数

参数：

* No arguments

返回值: 
输出用时，设置计时器全局变量为nil

示例:
```
(BF-time-end)
 ```

## BF-e->vla

说明：
重定义vlax-ename->vla-object函数

参数：

* ename - 图元名

返回值: 
vla对象

示例:
```
(BF-e->vla (car (entsel)))
 ```

## BF-vla->e

说明：
重定义vlax-vla-object->ename函数

参数：

* obj - vla对象名

返回值: 
图元名

示例:
```
(BF-vla->e obj)
 ```

## BF-save-system-variable

说明：
保存系统变量函数,保存当前的系统变量,为程序在非正常退出时恢复系统变量用

参数：

* a - 系统变量名组成的表(变量名  变量名 ....)

返回值: 
全局变量-*user-system-variable*-系统变量及其值组成的表((变量名 . 值) (...  ...))

示例:
```
(BF-save-system-variable '("cmdecho" "osmode" "dimtxt"))
 ```

## BF-reset-system-variable

说明：
恢复系统变量函数，和BF-save-system-variable成对使用

参数：

* No arguments

返回值: 
nil

示例:
```
(BF-reset-system-variable)
 ```

## BF-return

说明：
返回值函数，用于包装将要返回的值，主要作用还是为了含义更明确。

参数：

* value - 需要返回的值

返回值: 
返回值

示例:
```
(BF-return 1)
 ```

